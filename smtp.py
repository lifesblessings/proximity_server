import smtplib
import os

def sendemail(from_addr, to_addr_list, cc_addr_list, subject, message, login, password, smtpserver='smtp.gmail.com:587'):
    header  = 'From: %s' % from_addr
    header += 'To: %s' % ','.join(to_addr_list)
    header += 'Cc: %s' % ','.join(cc_addr_list)
    header += 'Subject: %s' % subject
    message = header + message
 
    server = smtplib.SMTP(smtpserver)
    server.starttls()
    server.login(login,password)
    problems = server.sendmail(from_addr, to_addr_list, message)
    server.quit()

def main():
    f = os.popen('ifconfig eth0 | grep "inet "')
    your_ip=f.read()
    f.close()
    sendemail('soporte@innovaxxion.com',['sbeeche@innovaxxion.com'],[],'Pi','\nRaspberry iniciada!\n'+your_ip[12:26],'soporte@innovaxxion.com','innova2k+1')

if __name__ == "__main__":
    main()
