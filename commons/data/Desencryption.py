
def HextoDecimal(fragment):
    integer = int(fragment[:2], 16)
    decimal = int(fragment[-2:], 16)/256
    if(integer > 127):
        return integer -256 + decimal
    else:
        return integer + decimal