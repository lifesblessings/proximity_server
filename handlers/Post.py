from handlers.Base import BaseHandler, DeniedHandler, role
import json
import socket
import base64
import io
import tornado.web
from commons.data.Desencryption import HextoDecimal
from datetime import datetime as dt
from os.path import (join, dirname)
from db.commons.Sql import insert_to_sql

class InformationHandler(BaseHandler):
    # @tornado.web.authenticated
    def post(self):
        prefix = "AC233F"
        beam1 = "C0227E"
        beam2 = "C02271"
        beac1 = "265D11"
        beac2 = "265D25"
        try:
            my_json = self.request.body.decode('utf8').replace("'", '"')
            my_json = json.loads(my_json)
            beamId = ""
            for js in my_json:
                if js["type"] == 'Gateway':
                    beamId = js["mac"]
                elif len(js["rawData"]) == 52 and js["mac"] == "".join(reversed([js["rawData"][40:][i:i+2] for i in range(0, len(js["rawData"][40:]), 2)])):
                    js["timestamp"] = dt.strptime(
                        js["timestamp"], '%Y-%m-%dT%H:%M:%SZ')
                    sensId = js["mac"]
                    try:
                        insert_to_sql(int(js["rssi"]),sensId,beamId,js["timestamp"],"prox_data")
                    except Exception as e:
                        print("exception:",e)
        except Exception as e:
            print("exception:",e)

