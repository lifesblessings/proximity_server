import tornado.escape
from urllib.parse import urlparse
from tornado.web import RequestHandler
import os
import json, socket
from mongoengine import *
from db.models.core.Users import User
from commons.utils.JsonManager import JsonManager


class BaseHandler(RequestHandler):
    def initialize(self):
        self.set_secure_cookie("flash","")
        cm = JsonManager(os.path.join(os.path.dirname(__file__), "../config/config_params.json"))
        if cm.data['local_concentrator']:
            self.local_concentrator = "visible"
        else:
            self.local_concentrator = "hidden"

    def get_login_url(self):
        return u"/auth/login"

    def get_current_user(self):
        user_json = self.get_secure_cookie("user")
        if user_json:
            return tornado.escape.json_decode(user_json)
        else:
            return None

    def get_concentrator(self):
        return self.local_concentrator

    # Allows us to get the previous URL
    def get_referring_url(self):
        try:
            _, _, referer, _, _, _ = urlparse.urlparse(self.request.headers.get('Referer'))
            if referer:
                return referer
        # Test code will throw this if there was no 'previous' page
        except AttributeError:
            pass
        return '/'

    def checkSession(self):
        if not self.get_current_user():
            self.set_status(401)
            self.clear_cookie("user")
            message = "Session Expirada"
            self.redirect("/")

    def get_flash(self):
        flash = self.get_secure_cookie('flash')
        self.clear_cookie('flash')
        return flash

    def get_essentials(self):
        mp = {k: ''.join(v) for k, v in self.request.arguments.iteritems()}

class DeniedHandler(BaseHandler):
    def get(self):
        hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
        self.render("denied.html", hostname=hostname, ip=ip, current_user=self.get_current_user(), local_concentrator=self.get_concentrator())

# decorator to check if the user has role permissions
# usage: @role("ReadWrite", "ReadOnly")
class role(BaseHandler):
    def __init__(self, *args):
        default = "Administrator"
        self.args = list(args)
        if default not in self.args:
            self.args.append(default)
    def __call__(self, f):
        roles = self.args
        def wrapped_f(self):
            current_user = self.get_current_user()
            if current_user:
                user = User.objects.get(username=self.get_current_user())
                if user:
                    if user.role in roles:
                        f(self)
                    else:
                        self.redirect("/denied")
            else:
                self.redirect("/")
        return wrapped_f
