import json
import uuid
import socket
import base64
import logging
import os, os.path
import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket
from tornado.tcpserver import TCPServer
from pprint import pprint
from mongoengine import connect
from tornado.options import define, options

# custom libs
from commons.utils.JsonManager import ( JsonManager )
from handlers.Session import( AuthLoginHandler, AuthLogoutHandler )
from handlers.Base import DeniedHandler
from handlers.Index import ( IndexHandler )
from handlers.areaf import ( AreaFHandler, DataWebSocket )
from handlers.areas import ( AreaSHandler, DataWebSockets )
from handlers.Post import ( InformationHandler )
from tornado.web import RequestHandler, Application
from tornado_http_auth import DigestAuthMixin, BasicAuthMixin, auth_required

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("user")

credentials = {'user1': 'pass1'}

#define("port", default=8000, help="run on the given port", type=int)
#define("port", default=443, help="run on the given port", type=int)

class Application(Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/index", IndexHandler),
            (r"/status", InformationHandler),
            (r"/websocket", DataWebSocket),
            (r"/websockets", DataWebSockets),
            (r"/areaf", AreaFHandler),
            (r"/areas", AreaSHandler),
        ]
        settings = dict(
            template_path=os.path.join(os.path.dirname(__file__), "templates"),
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=False,
            cookie_secret=base64.b64encode(uuid.uuid4().bytes + uuid.uuid4().bytes),
            debug=True,
            login_url="/auth/login"
        )
        super(Application, self).__init__(handlers, **settings)



class MainHandler(RequestHandler):
    def get(self):
        self.redirect('/auth/login')

#class MainHandler(BaseHandler):
#    @tornado.web.authenticated
#    def get(self):
#        self.write("login.html", username = self.get_current_user())

def main():
    cm = JsonManager(os.path.join(os.path.dirname(__file__), "config/config_params.json"))
    # connect to database in mongodb instance
    connect(cm.data['database']['name'], host='mongodb://'+cm.data['database']['ip'], port=cm.data['database']['port']);
    tornado.options.parse_command_line()
    app = Application()

    # implementation for SSL
    http_server = tornado.httpserver.HTTPServer(app, ssl_options={
        "certfile": os.path.join(os.path.dirname(__file__), cm.data["cert"]["crt"]),
        "keyfile": os.path.join(os.path.dirname(__file__), cm.data["cert"]["key"]),
    })
    http_server.listen(443)

    tornado.ioloop.IOLoop.current().start()
    tornado.autoreload.start()
    tornado.autoreload.watch('myfile')
    #tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()
