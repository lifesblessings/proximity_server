from mongoengine import *

class Credentials(Document):
    key       = StringField(required=True, unique=True)
    secret    = StringField(required=True)
