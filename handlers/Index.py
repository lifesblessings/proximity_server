from handlers.Base import BaseHandler, DeniedHandler, role
import socket
import base64
import io
import tornado.web
from tornado.websocket import WebSocketHandler, WebSocketClosedError
from tornado.ioloop import PeriodicCallback
import time
import datetime as dt
class IndexHandler(BaseHandler):
    # @tornado.web.authenticated
    def get(self):
        hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
        self.render("index.html", hostname=hostname, ip=ip,
                    current_user=self.get_current_user())


# cl = []
# stateA = 0
# stateB = 0

# class DataWebSocket(WebSocketHandler):
#     # @tornado.web.authenticated
#     def initialize(self):
#         print("socket init")
#         self.dataloop = None

#     def open(self):
#         if self not in cl:
#             cl.append(self)
#         print("WebSocket opened")

#     def on_message(self, message):
#         global stateA, stateB
#         # Start an infinite loop when this is called
#         if message == "get_data":
#             print("function: " + message)
#             try:
#                 self.dataloop = PeriodicCallback(self.loop, 2000)
#                 self.dataloop.start()
#             except WebSocketClosedError:
#                 self.dataloop.stop()
#                 print("dataloop error")
#         else:
#             print("Unsupported function: " + message)

#     def loop(self):
#         global stateA,stateB
#         try:
#             r = redis.Redis(host='localhost', port=6379, db=0)
#             ato1 = int(r.get("Ato1"))
#             ato2 = int(r.get("Ato2"))
#             bto1 = int(r.get("Bto1"))
#             bto2 = int(r.get("Bto2"))
#             #Estados: 1= Zona A, 2= Zona B, 0= Out of Zone
#             if ato1 > -50 and stateA==0:
#                 print("A near 1")
#                 stateA = 1
#                 self.write_message("A near 1," + str(dt.datetime.now()))
#             elif ato2 > -50 and stateA==0:
#                 print("A near 2")
#                 stateA = 2
#                 self.write_message("A near 2," + str(dt.datetime.now()))
#             elif ato1 < -50 and stateA==1:
#                 print("A out 1")
#                 stateA = 0
#                 self.write_message("A out 1," + str(dt.datetime.now())) 
#             elif ato2 < -50 and stateA==2:
#                 print("A out 2")
#                 stateA = 0
#                 self.write_message("A out 2," + str(dt.datetime.now()))

#             if bto1 > -50 and stateB==0:
#                 print("B near 1")
#                 stateB = 1
#                 self.write_message("B near 1," + str(dt.datetime.now()))
#             elif bto2 > -50 and stateB==0:
#                 print("B near 2")
#                 stateB = 2
#                 self.write_message("B near 2," + str(dt.datetime.now()))
#             elif bto1 < -50 and stateB==1:
#                 print("B out 1")
#                 stateB = 0
#                 self.write_message("B out 1," + str(dt.datetime.now()))
#             elif bto2 < -50 and stateB==2:
#                 print("B out 2")
#                 stateB = 0
#                 self.write_message("B out 2," + str(dt.datetime.now()))

#         except Exception as e:
#             print('exception in dataloop camera')
#             print(e)
# #             pass

#     def on_close(self):
#         if self in cl:
#             cl.remove(self)
#         self.dataloop.stop()
#         print("WebSocket closed")
