import base64
from bson.objectid import ObjectId
import os
import bcrypt
import hashlib
import urllib
import boto
import tornado.auth
import tornado.escape
import tornado.gen
import tornado.httpserver
import logging
import bson.json_util
import json
import time
import threading
import functools
#from PIL import Image
from tornado.ioloop import IOLoop
from tornado.web import asynchronous, RequestHandler, Application
from tornado.httpclient import AsyncHTTPClient
from handlers.Base import BaseHandler

_username='root'
_password='root'


# custom libs for db authentication
from db.models.core.Users import User

class AuthLoginHandler(BaseHandler):
    def get(self):
        notification = self.get_flash()
        if notification is None:
            notification = ""
        self.render("login.html", notification=notification)

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "").encode('utf8')
        try:
            user=User.objects.get(username__exact=username)
            print ("login", username)
            #print 'user exist'
            # Warning bcrypt will block IO loop:
            if bcrypt.hashpw(password, user.password.encode('utf8'))==user.password:
                #print 'user exist and the password ok'
                self.set_current_user(username)
                #print "current user", self.get_current_user()
                self.redirect(u"/index")
            else:
                #print 'user exist but the password is not ok'
                self.set_secure_cookie("flash", "* wrong password")
                self.redirect(u"/auth/login")

        except User.DoesNotExist:
            #print 'user does not exist'
            self.set_secure_cookie("flash", "* user does not exist")
            self.redirect(u"/auth/login")

    def set_current_user(self, user):
        print ("set_current_user:", user)
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")
        #print "after set curremnt user ", self.get_current_user()


class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(u"/auth/login")

class CheckSessionHandler(BaseHandler):
    def get(self):
        print ("checksession")
        if not self.get_current_user():
            self.set_status(401)
            self.clear_cookie("user")
            self.finish({"reason": 'session expired'})
            self.redirect(u"/auth/login")

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        User(username=username,password=bcrypt.hashpw(password, bcrypt.gensalt())).save()
