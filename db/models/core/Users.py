from mongoengine import *

class User(Document):
    username    = StringField(required=True, unique=True)
    password    = StringField(required=True)
    role        = StringField(required=True)
    name        = StringField(required=True)
    email       = StringField()
    tenantId    = ObjectIdField()
