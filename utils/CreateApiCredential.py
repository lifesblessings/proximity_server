import os
import json
import bcrypt
import getpass
import sys, getopt
from itertools import cycle
from datetime import datetime as dt

# mongo engine imports
from mongoengine import *

#MONGODB models
from db.models.core.Api import Credentials

def main():
   connect(cm.data['database']['name'], host=cm.data['database']['ip'], port=cm.data['database']['port'])

   # do some stuff in script
   username = raw_input('Ingrese nombre de usuario: ')
   pass1 = getpass.getpass('ingrese password: ')
   pass2 = getpass.getpass('reingrese password: ')
   count = 0

   roles = ['','ReadOnly','ReadWrite','Administrator']
   while True:
       if pass1==pass2:
           role = int(raw_input("Please enter role level: \n[1]:ReadOnly\n[2]:ReadWrite\n[3]:Administrator\n for Default[1] press enter:") or "1")
           if role >= 1 or role <= 3:
               role = roles[role]
               email = raw_input('Enter e-mail (optional): ')
               name = raw_input("Please enter name ["+username+"]:") or username
               user = User(username=username, password=bcrypt.hashpw(pass1.encode('utf8'), bcrypt.gensalt()), name=name, role=role, email=email)
               user.save()
               print 'Usuario creado',user.to_json()
           break
       else:
           if count <2:
               pass2 = getpass.getpass('password no coincide, reingrese nuevamente:')
               count+=1
           else:
               print "password no coincide, saliendo del programa."
               break

if __name__ == "__main__":
   main()
