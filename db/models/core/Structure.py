from db.models.core.Data import Features
from mongoengine import *


class SensorData(Document):
    sensorId    = ObjectIdField(required=True)
    beamerId    = ObjectIdField(required=True)
    data        = GenericEmbeddedDocumentField()
    rssi        = IntField(required=True)
    timestamp   = DateTimeField(required=True)
    dataType    = StringField(required=True)
    
class Sensor(Document):
    mac        = StringField(required=True)
    model      = StringField(required=True)
    brand      = StringField(required=True)
    tag        = StringField()
    features   = EmbeddedDocumentField(Features)
    createdAt  = DateTimeField(required=True)
    updatedAt  = DateTimeField()
    battery     = FloatField()
    voltage = IntField()
    temperature = FloatField()
    adv_count = IntField()
    secs_count = IntField()

class Tenant(Document):
    name        = StringField(required=True)
    createdAt   = DateTimeField(required=True)
    updatedAt   = DateTimeField()
    description = StringField()


class Beamer(Document):
    mac         = StringField(required=True)
    model       = StringField(required=True)
    brand       = StringField(required=True)
    tenantId    = ObjectIdField()
    createdAt   = DateTimeField(required=True)
    updatedAt   = DateTimeField()


