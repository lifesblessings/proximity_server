from mongoengine import *

class Vector3D(EmbeddedDocument):
    x      = FloatField(required=True)
    y      = FloatField(required=True)
    z      = FloatField(required=True)


class Accelerometer(EmbeddedDocument):
    acc    = EmbeddedDocumentField(Vector3D)
    


class UnknownData(EmbeddedDocument):
    rawData = StringField(required=True)


class Features(EmbeddedDocument):
    features = ListField()
    createdAt = DateTimeField()
    updatedAt = DateTimeField()

