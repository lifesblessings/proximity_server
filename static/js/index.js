
"use strict";
var ip_port = '';

var client = {
  // Connects to Pi via websocket
  connect: function (ipport) {
    var self = this;
    this.socket = new WebSocket("wss://" + ipport + "/websocket");
    // Request the video stream once connected
    this.socket.onopen = function () {
      console.log("Connected!");
      self.getData();
    };
    this.socket.onmessage = function (messageEvent) {
      console.log(messageEvent.data);
      var data_split = messageEvent.data.split(/[ ,]+/);
      var textbox1 = document.getElementById("textbox1");
      if (textbox1.children.length >= 20) { textbox1 = document.getElementById("textbox1").removeChild(textbox1.children[0]) }

      var textbox2 = document.getElementById("textbox2");
      if (textbox2.children.length >= 20) { textbox2 = document.getElementById("textbox2").removeChild(textbox2.children[0]) }

      var newline = "";
      //entró al área 1
      if (data_split[2] == '1') {
        //autorizado
        var newdiv = document.createElement('div');
        newdiv.classList.add('divs');
        if (data_split[0] == 'A') {       
          if (data_split[1] == 'near') {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Rodrigo entró en área Mantención - Autorizado";
          }
          else {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Rodrigo dejó el área Mantención - Autorizado";
          }
        }
        //no autorizado
        else if (data_split[0] == 'B') {
          newdiv.style.color = 'red';
          if (data_split[1] == 'near') {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Manuel entró en área Mantención - No Autorizado";
          }
          else {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Manuel dejó el área Mantención - No Autorizado";
          }
        }
        newdiv.innerText = newline;
        textbox1.appendChild(newdiv);
      }
      else if (data_split[2] == '2') {
        var newdiv = document.createElement('div');
        newdiv.classList.add('divs');
        if (data_split[0] == 'A') {
          newdiv.style.color = 'red';
          if (data_split[1] == 'near') {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Rodrigo entró en área Refinería - No Autorizado";
          }
          else {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Rodrigo dejó el área Refinería - No Autorizado";
          }
        }
        else if (data_split[0] == 'B') {
          if (data_split[1] == 'near') {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Manuel entró en área Refinería - Autorizado";
          }
          else {
            newline = data_split[3] + " " + data_split[4].substring(0,data_split[4].length-13) + " usuario Manuel dejó el área Refinería - Autorizado";
          }
        }
        newdiv.innerText = newline;
        textbox2.appendChild(newdiv);
      }
    };
  },
  // Requests stream
  getData: function () {
    this.socket.send("get_data");
  }
};

$(document).ready(function () {
  client.connect(location.host);
});