import tkinter
import matplotlib.pyplot as plt
import numpy as np
import pymysql

def scatterplot(x_data, y_data, x_label="", y_label="", title="", color = "r", yscale_log=False):

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x_data, range(len(y_data)), s = 10, color = color,marker = 's', label = x_label)
    ax.scatter(y_data, range(len(y_data)), s = 10, color = 'g', marker = 'o', label = y_label)
    plt.legend(loc='upper left')

    # Label the axes and provide a title
    ax.set_title(title)
    ax.set_xlabel('scatter')
    ax.set_ylabel('range')
    plt.show()

def get_data_beam2271(beacon):
        try:
            db = pymysql.connect(host="localhost", port=3306,
                                 user="Hoster", passwd="innova2k", db="proximity")
        except Exception as e:
            print("error tratando de conectar a DB")
            print(e)
        curs = db.cursor()
        query = "select rssi from prox_data where beamer='AC233FC02271' and sensor='" + beacon + "'"
        print(query)
        try:
            curs.execute(query)
        except Exception as e:
            print("exception:", e)
        data = []
        for val in curs.fetchall():
           data.append(val[0]) 
        db.close()
        return(data)

def get_data_beam227E(beacon):
        try:
            db = pymysql.connect(host="localhost", port=3306,
                                 user="Hoster", passwd="innova2k", db="proximity")
        except Exception as e:
            print("error tratando de conectar a DB")
            print(e)
        curs = db.cursor()
        query = "select rssi from prox_data where beamer='AC233FC0227E' and sensor='" + beacon + "'"
        print(query)
        try:
            curs.execute(query)
        except Exception as e:
            print("exception:", e)
        data = []
        for val in curs.fetchall():
           data.append(val[0])
        
        db.close()
        return(data)

def main():
    data_beam1 = get_data_beam227E('AC233F265D11')
    data_beam2 = get_data_beam2271('AC233F265D11')
    while(len(data_beam1)>len(data_beam2)): 
        data_beam1.pop()
    while(len(data_beam2)>len(data_beam1)): 
        data_beam2.pop()
    scatterplot(data_beam1,data_beam2,'beamer_near','beamer_far','plot comparativo')

if __name__ == "__main__":
    main()