import base64
from bson.objectid import ObjectId
import os
import bcrypt
import hashlib
import urllib

import boto
import cStringIO

import tornado.auth
import tornado.escape
import tornado.gen
import tornado.httpserver
import logging
import bson.json_util
import json
import urlparse
import time
import threading
import functools
from PIL import Image
from tornado.ioloop import IOLoop
from tornado.web import asynchronous, RequestHandler, Application
from tornado.httpclient import AsyncHTTPClient



from Base import BaseHandler
from db.models.core.Users import User

_username='root'
_password='root'

'''
class LoginPrevHandler(BaseHandler):
    def get(self):
        self.render("login.html", notification=self.get_flash())
    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "").encode('utf8')

        if username == _username and password == _password:
            print 'LOGIN SUCCESSFULL'
            self.set_current_user(username)
            self.redirect(u"/preview")
        else:
            print 'LOGIN UNSUCCESSFULL'
            self.set_secure_cookie('flash', "Login incorrect")
            self.redirect(u"/login")

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")
'''

class LoginHandler(BaseHandler):

    def get(self):
        notification = self.get_flash()
        if notification is None:
            notification = ""
        self.render("login.html", notification=notification)

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "").encode('utf8')

        try:
            user=User.objects.get(username__exact=username)
            # print 'user exist'
            # Warning bcrypt will block IO loop:
            if bcrypt.hashpw(password, user.password.encode('utf8'))==user.password:
                #print 'user exist and the password ok'
                self.set_current_user(username)
                self.redirect(u"/preview")
            else:
                self.set_secure_cookie("flash","* wrong user or pass")
                self.redirect(u"/login")
        except User.DoesNotExist:
            #print 'user does not exist'
            self.set_secure_cookie("flash", "* user does not exist")
            self.redirect(u"/login")

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", tornado.escape.json_encode(user))
        else:
            self.clear_cookie("user")


class LogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(u"/login")

class CheckSessionHandler(BaseHandler):
    def get(self):
        if not self.get_current_user():
            self.set_status(401)
            self.clear_cookie("user")
            self.finish({"reason": 'session expired'})
            self.redirect(u"/login")


class RegisterHandler(LoginHandler):
    def get(self):
        self.render("register.html", next=self.get_argument("next", "/"))

    def post(self):
        username = self.get_argument("username", "")
        password = self.get_argument("password", "")
        User(username=username,password=bcrypt.hashpw(password, bcrypt.gensalt())).save()

        #already_taken = User.objects(username=username)
        #print already_taken == username

        #if already_taken:
        #    error_msg = u"?error=" + tornado.escape.url_escape("Login name already taken")
        #    self.redirect(u"/login" + error_msg)

        # Warning bcrypt will block IO loop:
        #password = self.get_argument("password", "").encode('utf-8')
        #hashed_pass = bcrypt.hashpw(password, bcrypt.gensalt(8))

        #User(username=username,password=hashed_pass).save()

        #auth = self.application.syncdb['users'].save(user)
        #self.set_current_user(username)

        #self.redirect("/dashboard")
