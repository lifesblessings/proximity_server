from mongoengine import ( Document, StringField, EmbeddedDocumentField, DateTimeField, IntField )
from db.commons.Data import Histogram

class PositionReport(Document):
    sensorId  = StringField(required=True)
    networkId = StringField()
    odistance = IntField(required=True)
    ts        = DateTimeField(required=True)
