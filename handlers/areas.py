from handlers.Base import BaseHandler, DeniedHandler, role
import socket
import base64
import io
import tornado.web
from db.commons.Sql import *
from tornado.websocket import WebSocketHandler, WebSocketClosedError
from tornado.ioloop import PeriodicCallback
import time
import datetime as dt
import pytz

class AreaSHandler(BaseHandler):
    # @tornado.web.authenticated
    def get(self):
        hostname = socket.gethostname()
        ip = socket.gethostbyname(hostname)
        self.render("areas.html", hostname=hostname, ip=ip,
                    current_user=self.get_current_user())


cl = []
stateA = 0
stateB = 0
stateAbf = 0
stateBbf = 0
ADate = ""
BDate = ""

class DataWebSockets(WebSocketHandler):
    # @tornado.web.authenticated
    def initialize(self):
        print("socket init")
        self.dataloop = None

    def open(self):
        if self not in cl:
            cl.append(self)
        print("WebSocket opened")

    def on_message(self, message):
        global stateA, stateB
        # Start an infinite loop when this is called
        if message == "get_data":
            print("function: " + message)
            try:
                self.loop()
                self.dataloop = PeriodicCallback(self.loop, 5000)
                self.dataloop.start()
            except WebSocketClosedError:
                self.dataloop.stop()
                print("dataloop error")
        else:
            print("Unsupported function: " + message)

    def send_message(self, message):
        for c in cl:
            c.write_message(message)

    def loop(self):
        global stateA,stateB,stateAbf,stateBbf,ADate,BDate,cl
        try:
            if not self.dataloop:
                if(stateA==1): self.write_message("A near 1," + ADate)
                elif(stateA==2): self.write_message("A near 2," + ADate)
                if(stateB==1): self.write_message("B near 1," + BDate)
                elif(stateB==2): self.write_message("B near 2," + BDate)

            ato1,ato2,bto1,bto2 = process_data('AC233F265D25','AC233F265D11',6)
            print("ato1 ato2",ato1,ato2)
            print("bto1 bto2",bto1,bto2)
            print("stateA and before",stateA,stateAbf)
            print("stateB and before",stateB,stateBbf)
            date_now = str(dt.datetime.now(pytz.timezone("America/Santiago")))
#Estados: 1= Zona 1, 2= Zona 2, 0= Out of Zone,3=corroborating 1,4=corroborating 2
#Máquina de estados beacon A
            if stateA==1:
                if ato1 > -55:
                    pass
                elif ato1 < -55:
                    print("A middle 1")
                    stateA = 3
                    stateAbf = 1
            elif stateA==3:
                if ato1 > -55:
                    if stateAbf == 1:
                        stateA = 1
                        stateAbf = 3
                    elif stateAbf == 0:
                        stateA=1
                        print("A near 1")
                        stateAbf = 3
                        self.send_message("A near 1," + date_now)
                        ADate = date_now
                elif ato1 < -55:
                    if stateAbf == 0:
                        stateA = 0
                        stateAbf = 3
                    elif stateAbf == 1:
                        stateA = 0
                        stateAbf = 3
                        print("A out 1")
                        self.send_message("A out 1," + date_now)
            elif stateA==0:
                if ato1 > -55:
                    print("A middle 1")
                    stateA = 3
                    stateAbf = 0
                elif ato2 > -55:
                    print("A middle 2")
                    stateA = 4
                    stateAbf = 0
            elif stateA==4:
                if ato2 > -55:
                    if stateAbf == 2:
                        stateA = 2
                        stateAbf = 4
                    elif stateAbf == 0:
                        stateA=2
                        print("A near 2")
                        stateAbf = 4
                        self.send_message("A near 2," + date_now)
                        ADate = date_now
                elif ato2 < -55:
                    if stateAbf == 0:
                        stateA = 0
                        stateAbf = 4
                    elif stateAbf == 2:
                        stateA = 0
                        stateAbf = 4
                        print("A out 2")
                        self.send_message("A out 2," + date_now)
            elif stateA==2: 
                if ato2 > -55:
                    pass
                elif ato2 < -55:
                    print("A middle 2")
                    stateA = 4
                    stateAbf = 2
#Máquina de estados beacon B
            if stateB==1:
                if bto1 > -55:
                    pass
                elif bto1 < -55:
                    print("B middle 1")
                    stateB = 3
                    stateBbf = 1
            elif stateB==3:
                if bto1 > -55:
                    if stateBbf == 1:
                        stateB = 1
                        stateBbf = 3
                    elif stateBbf == 0:
                        stateB=1
                        print("B near 1")
                        stateBbf = 3
                        self.send_message("B near 1," + date_now)
                        BDate = date_now
                elif bto1 < -55:
                    if stateBbf == 0:
                        stateB = 0
                        stateBbf = 3
                    elif stateBbf == 1:
                        stateB = 0
                        stateBbf = 3
                        print("B out 1")
                        self.send_message("B out 1," + date_now)
            elif stateB==0:
                if bto1 > -55:
                    print("B middle 1")
                    stateB = 3
                    stateBbf = 0
                elif bto2 > -55:
                    print("B middle 2")
                    stateB = 4
                    stateBbf = 0
            elif stateB==4:
                if bto2 > -55:
                    if stateBbf == 2:
                        stateB = 2
                        stateBbf = 4
                    elif stateBbf == 0:
                        stateB=2
                        print("B near 2")
                        stateBbf = 4
                        self.send_message("B near 2," + date_now)
                        BDate = date_now
                elif bto2 < -55:
                    if stateBbf == 0:
                        stateB = 0
                        stateBbf = 4
                    elif stateBbf == 2:
                        stateB = 0
                        stateBbf = 4
                        print("B out 2")
                        self.send_message("B out 2," + date_now)
            elif stateB==2: 
                if bto2 > -55:
                    pass
                elif bto2 < -55:
                    print("B middle 2")
                    stateB = 4
                    stateBbf = 2

        except Exception as e:
            print('exception in dataloop camera')
            print(e)
            pass

    def on_close(self):
        if self in cl:
            cl.remove(self)
        self.dataloop.stop()
        print("WebSocket closed")
