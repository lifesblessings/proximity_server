import pymysql
from datetime import datetime as dt

beam1 = "AC233FC0227E"
beam2 = "AC233FC02271"
beac1 = "AC233F265D11"
beac2 = "AC233F265D25"


def Average(lst):
    return sum(lst) / len(lst)

def get_last_states(sensor):
    try:
        db = pymysql.connect(host="localhost", port=3306,
                             user="Hoster", passwd="innova2k", db="proximity")
    except Exception as e:
        print("error tratando de conectar a DB")
        print(e)
    curs = db.cursor()
    query = "SELECT state,time FROM state_data WHERE sensor='" + sensor + "' ORDER BY ID DESC LIMIT 2"
    try:
        curs.execute(query)
    except Exception as e:
        print("exception:", e)
    return curs.fetchall()


def insert_to_sql(rssi, sensor, beamer, time, table):
    try:
        db = pymysql.connect(host="localhost", port=3306,
                             user="Hoster", passwd="innova2k", db="proximity")
    except Exception as e:
        print("error tratando de conectar a DB")
        print(e)
    curs = db.cursor()
    if(table == "prox_data"):
        query = "INSERT INTO prox_data (sensor,beamer,rssi,time) VALUES('" + \
            sensor + "','" + beamer + "','" + \
                str(rssi) + "','" + str(time) + "')"
    else:
        query = "INSERT INTO state_data (sensor,state,time) VALUES('" + sensor + "','" + str(rssi) + "','" + str(time) + "')"
    try:
        curs.execute(query)
    except Exception as e:
        print("exception:", e)

    db.commit()
    db.close()


def get_data_beam2271(beacon, limit):
    try:
        db = pymysql.connect(host="localhost", port=3306,
                             user="Hoster", passwd="innova2k", db="proximity")
    except Exception as e:
        print("error tratando de conectar a DB")
        print(e)
    curs = db.cursor()
    query = "select rssi from prox_data where beamer='AC233FC02271' and sensor='" +  beacon + "' order by id desc limit " + str(limit)
    #    beacon + "' and time >= now() - interval " + str(limit) + \
    #    " second order by id desc"
    try:
        curs.execute(query)
    except Exception as e:
        print("exception:", e)
    data = []
    for val in curs.fetchall():
        data.append(int(val[0]))
    db.close()
    return(data)


def get_data_beam227E(beacon, limit):
    try:
        db = pymysql.connect(host="localhost", port=3306,
                             user="Hoster", passwd="innova2k", db="proximity")
    except Exception as e:
        print("error tratando de conectar a DB")
        print(e)
    curs = db.cursor()
    #query = "select rssi from prox_data where beamer='AC233FC0227E' and sensor='" + \
    #    beacon + "' and time >= now() - interval " + str(limit) + \
    #    " second order by id desc"
    query = "select rssi from prox_data where beamer='AC233FC0227E' and sensor='" +  beacon + "' order by id desc limit " + str(limit)
    try:
        curs.execute(query)
    except Exception as e:
        print("exception:", e)
    data = []
    for val in curs.fetchall():
        data.append(int(val[0]))

    db.close()
    return(data)


def process_data(beacon1, beacon2, avg_count):
    beac1_to227E = get_data_beam227E(beacon1, avg_count)
    beac1_to2271 = get_data_beam2271(beacon1, avg_count)
    beac2_to227E = get_data_beam227E(beacon2, avg_count)
    beac2_to2271 = get_data_beam2271(beacon2, avg_count)
    #process_states(Average(beac1_to227E), Average(beac1_to2271), Average(beac2_to227E), Average(beac2_to2271))
    return (Average(beac1_to227E), Average(beac1_to2271), Average(beac2_to227E), Average(beac2_to2271))

# def process_one(ato1,ato2,sensor,beamer1,beamer2):
#     lis = get_last_states(sensor)    
#     stateA = lis[0]
#     stateABf = lis[1]
#     stateA = get_last_states

#     if stateA == 1:
#         if ato1 > -55:
#             pass
#         elif ato1 < -55:
#             print("A middle 1")
#             stateA = 3
#             stateAbf = 1
#     elif stateA == 3 or stateA == 4:
#         if ato1 > -55:
#             if stateAbf == 1:
#                 return 1
#             elif stateAbf == 0:
#                 return 1
#                 print("A near 1")
#         elif ato1 < -55:
#             if stateAbf == 0:
#                 stateA = 0
#                 stateAbf = 3
#             elif stateAbf == 1:
#                 stateA = 0
#                 stateAbf = 3
#                 print("A out 1")
#                 elif stateA == 0:
#         if ato1 > -55:
#             print("A middle 1")
#             stateA = 3
#             stateAbf = 0
#         elif ato2 > -55:
#             print("A middle 2")
#             stateA = 4
#             stateAbf = 0
#     elif stateA == 4:
#         if ato2 > -55:
#             if stateAbf == 2:
#                 stateA = 2
#                 stateAbf = 4
#             elif stateAbf == 0:
#                 stateA = 2
#                 print("A near 2")
#                 stateAbf = 4                           
#         elif ato2 < -55:
#             if stateAbf == 0:
#                 stateA = 0
#                 stateAbf = 4
#             elif stateAbf == 2:
#                 stateA = 0
#                 stateAbf = 4
#                 print("A out 2")
#                 elif stateA == 2:
#         if ato2 > -55:
#             pass
#         elif ato2 < -55:
#             print("A middle 2")
#             stateA = 4
#             stateAbf = 2

# def process_states(beac1_tobeam1, beac1_tobeam2, beac2_tobeam1, beac2_tobeam2):
    
#     process_one(beac1_tobeam1,beac1_tobeam2,beac1,beam1,beam2)

    


def insert_into_state(beac1_tobeam1, beac1_tobeam2, beac2_tobeam1, beac2_tobeam2):
    insert_to_sql(beac1_tobeam1, beam1, beac1, dt.datetime.now(), "state_data")
    insert_to_sql(beac1_tobeam2, beam1, beac2, dt.datetime.now(), "state_data")
    insert_to_sql(beac2_tobeam1, beam2, beac1, dt.datetime.now(), "state_data")
    insert_to_sql(beac2_tobeam2, beam2, beac2, dt.datetime.now(), "state_data")
