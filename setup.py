from setuptools import find_packages, setup

setup(
    name='3da_sensor',
    version='1.0',
    packages=find_packages(exclude=['static,templates']),
    zip_safe=True,
    decription='',
    entry_points={}
)
